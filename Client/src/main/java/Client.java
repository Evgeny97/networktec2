//Created by Evgeny on 14.09.2017.

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class Client {
    public static void main(String[] args) throws IOException {
        File file = new File(args[0]);
        try (BufferedInputStream fromFileStream = new BufferedInputStream(new FileInputStream(file))) {
            InetAddress ipAddress = InetAddress.getByName(args[1]);
            try (Socket client = new Socket(ipAddress, Integer.valueOf(args[2]))) {
                try (BufferedOutputStream toServerStream = new BufferedOutputStream(client.getOutputStream())) {
                    BufferedInputStream fromServerStream = new BufferedInputStream(client.getInputStream());
                    byte[] fileNameByteArray = file.getName().getBytes(StandardCharsets.UTF_8);
                    byte[] fileNameLengthInByteArray = ByteBuffer.allocate(Integer.BYTES).putInt(fileNameByteArray.length).array();
                    byte[] longInBytes = ByteBuffer.allocate(Long.BYTES).putLong(file.length()).array();
                    toServerStream.write(fileNameLengthInByteArray);
                    toServerStream.write(fileNameByteArray);
                    toServerStream.write(longInBytes);
                    toServerStream.flush();

                    byte[] byteArray = new byte[2048];
                    int readCount;
                    long sent = 0;
//                    while (true) {
//                        toServerStream.write(byteArray);
//                    }

                    while ((readCount = fromFileStream.read(byteArray)) != -1) {
                        toServerStream.write(byteArray, 0, readCount);
                        sent += readCount;
                    }
                    toServerStream.flush();
                    System.out.println(sent);
                    client.setSoTimeout(5000);
                    int returnCode;
                    try {
                        returnCode = fromServerStream.read();
                    } catch (SocketTimeoutException e) {
                        returnCode = 1;
                    }
                    System.out.println((returnCode == 0) ? "File send finish" : "Some error");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
