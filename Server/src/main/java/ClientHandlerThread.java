//Created by Evgeny on 20.09.2017.

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class ClientHandlerThread extends Thread {
    private Socket clientSocket;

    ClientHandlerThread(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        byte[] buf = new byte[2048];
        try (BufferedInputStream fromClientStream = new BufferedInputStream(clientSocket.getInputStream());
             BufferedOutputStream toClientStream = new BufferedOutputStream(clientSocket.getOutputStream())) {
            byte[] lengthOfFileNameByteArray = new byte[Integer.BYTES];

            int receivedBytes = 0;
            while (receivedBytes < Integer.BYTES) {
                receivedBytes += fromClientStream.read(lengthOfFileNameByteArray, receivedBytes, Integer.BYTES - receivedBytes);
            }
            int fileNameLength = ByteBuffer.wrap(lengthOfFileNameByteArray).getInt();

            receivedBytes = 0;
            byte[] fileNameByteArray = new byte[fileNameLength];
            while (receivedBytes < fileNameLength) {
                receivedBytes += fromClientStream.read(fileNameByteArray, receivedBytes, fileNameLength - receivedBytes);
            }
            String fileName = new String(fileNameByteArray, StandardCharsets.UTF_8);
            fileName = parseFileName(fileName);


            receivedBytes = 0;
            byte[] longInByteArray = new byte[Long.BYTES];
            while (receivedBytes < Long.BYTES) {
                receivedBytes += fromClientStream.read(longInByteArray, receivedBytes, Long.BYTES - receivedBytes);
            }

            long sizeOfFile = ByteBuffer.wrap(longInByteArray).getLong();

            System.out.println(sizeOfFile);
            long alreadyGet = 0;
            long readFor3Sec = 0;
            try (BufferedOutputStream toFileStream = new BufferedOutputStream(new FileOutputStream("uploads/" + fileName))) {
                int readCount;
                long startTime = System.currentTimeMillis();
                long start3SecTime = startTime;

                while (alreadyGet < sizeOfFile) {
                    readCount = fromClientStream.read(buf);
                    alreadyGet += readCount;
                    readFor3Sec += readCount;
                    toFileStream.write(buf, 0, readCount);
                    if (start3SecTime + 3000 < System.currentTimeMillis()) {
                        double averageSpeed = (double) alreadyGet / ((System.currentTimeMillis() - startTime) * 1000);
                        double curSpeed = (double) readFor3Sec / ((System.currentTimeMillis() - start3SecTime) * 1000);
                        System.out.println("Average speed: " + averageSpeed + " byte/sec");
                        System.out.println("Current speed: " + curSpeed + " byte/sec");
                        readFor3Sec = 0;
                        start3SecTime = System.currentTimeMillis();
                    }
                }
                double averageSpeed = (double) alreadyGet / ((System.currentTimeMillis() - startTime) * 1000);
                double curSpeed = (double) readFor3Sec / ((System.currentTimeMillis() - start3SecTime) * 1000);
                System.out.println("Average speed: " + averageSpeed + " byte/sec");
                System.out.println("Current speed: " + curSpeed + " byte/sec");
            }
            System.out.println("File accepted");
            toClientStream.write(0);
            toClientStream.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (clientSocket != null && !clientSocket.isClosed()) {
                clientSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String parseFileName(String name) {
        name = name.trim();
        int index = name.lastIndexOf("/");
        if (index == -1) {
            index = name.lastIndexOf("\\");
            if (index == -1) {
                return name;
            }
        }
        return name.substring(index + 1);
    }
}
