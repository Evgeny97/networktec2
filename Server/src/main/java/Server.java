//Created by Evgeny on 14.09.2017.

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        System.out.println("Server side");
        try (ServerSocket serverSocket = new ServerSocket(Integer.valueOf(args[0]))) {
            try {
                while (true) {
                    System.out.println("Waiting for a client...");
                    Socket fromClient = serverSocket.accept();
                    ClientHandlerThread clientHandlerThread = new ClientHandlerThread(fromClient);
                    System.out.println("Client connected");
                    clientHandlerThread.start();
                }
            } catch (IOException e) {
                System.out.println("Can't accept");
            }
        } catch (IOException e) {
            System.out.println("Couldn't listen to port 4444");
        }
    }
}
